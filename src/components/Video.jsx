
import {Link, BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import SelectTopic from './SelectTopic'
import React from 'react'
 
const animateCategory = [   {name: 'Action', path: '/video/animation/action'},
                            {name: 'Romance', path: '/video/animation/romance'},
                            {name: 'Comedy', path: '/video/animation/comedy'}]

 const movieCategory = [    {name: 'Adventure', path: '/video/movie/adventure'},
                            {name: 'Comedy', path: '/video/movie/comedy'},
                            {name: 'Crime', path: '/video/movie/crime'},
                            {name: 'Documentary', path: '/video/movie/documentary'}]                            

export default function Video(props) {
    console.log(props.match.params.animateCategory);
    
    return (
        <div>
            <Router>
                <div className='container' style={{marginTop: '20px'}}>
                    <h5>Two Type of Videos:</h5>
                </div> 
                <div style={{paddingLeft: '50px'}} className="container">
                    <ul style={{display: 'inline'}}>
                        <li style={{fontSize: '20px'}}><Link to="/video/animation">Animation</Link></li>
                        <li style={{fontSize: '20px'}}><Link to="/video/movie">Movie</Link></li>
                    </ul>
                 <Switch>
                    <Route exact path="/video" component={SelectTopic}/>  
                    <Route path="/video/animation" component={AnimateTopics}/>
                    <Route path="/video/movie" component={MovieTopics}/>
                </Switch>
                </div>
               
            </Router>
            
        </div>
    )
}



function AnimateTopics(){
    return (
        <Router>
            <div>
                <div className='container' style={{marginTop: '20px'}}>
                    <h5>Animation Catagory:</h5>
                </div> 
                <div className="container" style={{paddingLeft: '50px'}}>
                <ul style={{display: 'inline'}}>
                    {animateCategory.map((animate) => 
                    <li style={{fontSize: '20px'}}>
                        <Link to={animate.path}>{animate.name}</Link>
                    </li>)}
                </ul>
                </div> 
            </div>    
            <Switch>
                <Route exact path="/video/animation" component={SelectTopic}/>      
                <Route path="/video/animation/action" component={Action}/>
                <Route path="/video/animation/romance" component={Romance}/>
                <Route path="/video/animation/comedy" component={animateComedy}/>
            </Switch>    
        </Router>
    )
}

function MovieTopics(){
    return (
        <Router>
            <div>
                <div className='container' style={{marginTop: '20px'}}>
                    <h5>Movies Catagory:</h5>
                </div> 
                <div className="container" style={{paddingLeft: '50px'}}>
                <ul style={{display: 'inline'}}>
                    {movieCategory.map((movie) => 
                    <li style={{fontSize: '20px'}}>
                        <Link to={movie.path}>{movie.name}</Link>
                    </li>)}
                </ul>
                </div> 
            </div>    
            <Switch>
                <Route exact path="/video/movie" component={SelectTopic}/> 
                <Route path="/video/movie/adventure" component={Adventure}/>
                <Route path="/video/movie/comedy" component={movieComedy}/>
                <Route path="/video/movie/crime" component={Crime}/>
                <Route path="/video/movie/documentary" component={Documentary}/>
            </Switch>    
        </Router>
    )
}


function Action(){
    return (
        <div className='container' style={{marginTop: '20px', textAlign: 'center'}}>
            <h3>Action Animate</h3>
        </div>
    )
}

function Romance(){
    return (
        <div className='container' style={{marginTop: '20px', textAlign: 'center'}}>
            <h3>Romance Animate</h3>
        </div>
    )
}

function animateComedy(){
    return (
        <div className='container' style={{marginTop: '20px', textAlign: 'center'}}>
            <h3>Comedy Animate</h3>
        </div>
    )
}

function movieComedy(){
    return (
        <div className='container' style={{marginTop: '20px', textAlign: 'center'}}>
            <h3>Comedy Movie</h3>
        </div>
    )
}

function Adventure(){
    return (
        <div className='container' style={{marginTop: '20px', textAlign: 'center'}}>
            <h3>Adventure Movie</h3>
        </div>
    )
}

function Crime(){
    return (
        <div className='container' style={{marginTop: '20px', textAlign: 'center'}}>
            <h3>Crime Movie</h3>
        </div>
    )
}


function Documentary(){
    return (
        <div className='container' style={{marginTop: '20px', textAlign: 'center'}}>
            <h3>Documentary Movie</h3>
        </div>
    )
}
