import {Link, BrowserRouter as Router , Route, Switch} from 'react-router-dom'
import React, { Component } from 'react'
import Home from './Home'
import Auth from './Auth'

export default class AnimateTopic extends Component {
    constructor(){
        super()
        this.state = {
            animateTopic: [{action: 'action'},{romance: 'ramance'},{comedy: 'comedy'}]
        }
    }
    render() {
        return (
            <Router>
                <div>
                    <div className='container' style={{marginTop: '20px'}}>
                        <h5>Animation Catagory:</h5>
                    </div> 
                    <div className="container" style={{paddingLeft: '50px'}}>
                    <ul style={{display: 'inline'}}>
                        <li style={{fontSize: '20px'}}>
                            <Link to="/video/animation/action">Action</Link>
                        </li>
                        <li style={{fontSize: '20px'}}>
                            <Link to="/video/movie/romance">Romance</Link>
                        </li>
                        <li style={{fontSize: '20px'}}>
                            <Link to="/video/movie/comedy">Comedy</Link>
                        </li>   
                    </ul>
                    </div>
                    <div className='container' style={{marginTop: '20px'}}>
                        <h5>Plese Select a Topic.</h5>
                    </div>    
                </div>
                <Switch>
                    <Route path="/video/animation/action" component={Auth}/>
                    <Route path=""/>
                </Switch>
            </Router>
        )
    }
}