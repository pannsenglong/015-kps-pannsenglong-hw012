

import {Form , Button, Container} from 'react-bootstrap'
import {BrowserRouter as Router, Route,  Switch, Link} from 'react-router-dom'
import Wellcome from './Wellcome'

import React from 'react'

export default function Auth(props) {
    return(
        <Router>
             <Switch>                 
                 <Route path="/auth" exact component={Login}/>
                 <Route path="/wellcome" component={Wellcome}/>
             </Switch>       
          </Router>
    )
}

function Login (){
    return (
        <Container style={{marginTop: '50px'}}>
            <Form >
                <Form.Group controlId="formBasicEmail">
                    <Form.Label style={{fontWeight: 'bold'}}>User Name</Form.Label>
                    <Form.Control type="text" placeholder="Enter User Name" />
                    <Form.Text className="text-muted">
                        We'll never share your username with anyone else.
                    </Form.Text>
                </Form.Group>
                <Form.Group controlId="formBasicPassword">
                    <Form.Label style={{fontWeight: 'bold'}}>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" />
                </Form.Group>
                <Form.Group controlId="formBasicCheckbox">
                    <Form.Check type="checkbox" label="Check me out" />
                </Form.Group>
                <Link to="/wellcome">
                    <Button variant="dark" type="submit">Login</Button>    
                </Link>
            </Form>
        </Container>
    )
}


