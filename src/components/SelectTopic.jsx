import React from 'react'

export default function SelectTopic() {
    return (
        <div className='container' style={{marginTop: '20px'}}>
            <h5>Please Select a Topic.</h5>
        </div>
    )
}
