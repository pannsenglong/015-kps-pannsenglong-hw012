import React from 'react'

export default function Post(post) {
    console.log(post.match.params.id);
    let id = post.match.params.id
    
    return (
        <div style={{textAlign: 'center', marginTop: '200px'}}>
            <h3>This is content from post {id}</h3>
        </div>
    )
}
