import React from 'react'
import {Link} from 'react-router-dom'


const account =   [     {name: 'Netflix', path: '/account?name=netflix'},
                        {name: 'Zilow Group', path: '/account?name=zilow_group'},
                        {name: 'Yahoo', path: '/account?name=yahoo'},
                        {name: 'Modus Create', path: '/account?name=modus_create'}]      

export default function Account(props) { 
    const queryString = new URLSearchParams(props.location.search)
    console.log(queryString.get("name"));
    return (
            <div>
                <div className='container' style={{marginTop: '20px'}}>
                    <h5>Accounts:</h5>
                </div> 
                <div className="container" style={{paddingLeft: '50px'}}>
                <ul style={{display: 'inline'}}>
                    {account.map((animate) => 
                    <li style={{fontSize: '20px'}}>
                        <Link to={animate.path}>{animate.name}</Link>
                    </li>)}
                </ul>
                <RenderName name={queryString.get("name")}/>
                </div> 
            </div>    
    )
}

function RenderName({name}){
    return (
        <div>
            {name === null?(<div className='container' style={{marginTop: '20px'}}>
            <h5>There is no name in the query string.</h5>
            </div>)
            :
            <div className='container' style={{marginTop: '20px'}}>
                <h5>The <span style={{color: 'red'}}>name</span>  in the query string is " <span style={{color: 'red'}}>{name}</span> ".</h5>
            </div>}
        </div>
    )
}
