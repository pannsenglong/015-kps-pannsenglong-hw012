import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Card, Row, Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'


export default function Home(props) {
    console.log(props.data);  
    let cardItem = props.data.map((datas) => 
                <Col col md={4} xl={3} style={{marginTop: '30px', padding: '0px 25px'}} >
                    <Card style={{  border: '1px solid grey', borderRadius: '10px',  boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)', textAlign: 'center'}}>
                        <Card.Img style={{borderRadius: '10px'}} variant="top" src= {datas.image} />
                        <Card.Body>
                            <Card.Title>{datas.title}</Card.Title>
                            <Card.Text>
                            {datas.description}
                            </Card.Text>
                            <Link to={`/post/${datas.id}`}>See More >></Link>
                        </Card.Body>
                        <p style={{padding: '10px 0px', backgroundColor: '#f8f9fa'}}>Last update 1mn ago</p>
                    </Card> 
                </Col>
    ); 

    return (
          <div className="container-fluid">
              <Row style={{textAlign: 'center'}}>
                    {cardItem} 
              </Row>
          </div>
    )
}
