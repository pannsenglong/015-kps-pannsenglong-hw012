import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Navbar, Nav, Button, Form, FormControl} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import '../styles/menu_style.css'

export default class Menu extends Component {
    render() {
        return (
            <div>    
                <Navbar bg="light" expand="lg">
                    <div className="container">
                    <Navbar.Brand as={Link} to="/" style={{fontWeight: 'bold'}}>React-Router</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                        <Nav.Link  className="option" as={Link} to="/home" style={{color: 'black',}}>Home</Nav.Link>
                        <Nav.Link className="option" as={Link} to="/video" style={{color: 'black'}}>Video</Nav.Link>
                        <Nav.Link className="option" as={Link} to="/account" style={{color: 'black'}}>Account</Nav.Link>
                        <Nav.Link className="option" as={Link} to="/auth" style={{color: 'black'}}>Auth</Nav.Link>
                        </Nav>
                        <Form inline>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                        <Button variant="outline-success">Search</Button>
                        </Form>
                    </Navbar.Collapse>
                    </div>
                </Navbar>  
            </div>
        )
    }
}
