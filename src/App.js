import React, {Component} from 'react';
import './App.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import Menu from './components/Menu'
import Account from './components/Account'
import Video from './components/Video'
import Auth from './components/Auth';
import Home from './components/Home'
import Post from './components/Post'
import Main_Page from './components/Main_Page';
import Wellcome from './components/Wellcome';




export default class App extends Component {
  constructor(){
    super();
    this.state = {
      data: [{id: 1 , image: 'https://dailymedia.investagrams.com/investadaily/2019/04/Avenger-Traders-2.jpg', title: 'Avenger', description: 'In the years since Tony Stark told the world he was Iron Man, the Marvel Cinematic Universe ...'},
             {id: 2 ,image: 'https://dailymedia.investagrams.com/investadaily/2019/04/Avenger-Traders-2.jpg', title: 'Avenger', description: 'In the years since Tony Stark told the world he was Iron Man, the Marvel Cinematic Universe ...'},
             {id: 3 ,image: 'https://dailymedia.investagrams.com/investadaily/2019/04/Avenger-Traders-2.jpg', title: 'Avenger', description: 'In the years since Tony Stark told the world he was Iron Man, the Marvel Cinematic Universe ...'},
             {id: 4 ,image: 'https://dailymedia.investagrams.com/investadaily/2019/04/Avenger-Traders-2.jpg', title: 'Avenger', description: 'In the years since Tony Stark told the world he was Iron Man, the Marvel Cinematic Universe ...'},
             {id: 5 ,image: 'https://dailymedia.investagrams.com/investadaily/2019/04/Avenger-Traders-2.jpg', title: 'Avenger', description: 'In the years since Tony Stark told the world he was Iron Man, the Marvel Cinematic Universe ...'},
             {id: 6 ,image: 'https://dailymedia.investagrams.com/investadaily/2019/04/Avenger-Traders-2.jpg', title: 'Avenger', description: 'In the years since Tony Stark told the world he was Iron Man, the Marvel Cinematic Universe ...'},
             {id: 7 ,image: 'https://dailymedia.investagrams.com/investadaily/2019/04/Avenger-Traders-2.jpg', title: 'Avenger', description: 'In the years since Tony Stark told the world he was Iron Man, the Marvel Cinematic Universe ...'},
             {id: 8 ,image: 'https://dailymedia.investagrams.com/investadaily/2019/04/Avenger-Traders-2.jpg', title: 'Avenger', description: 'In the years since Tony Stark told the world he was Iron Man, the Marvel Cinematic Universe ...'},
             {id: 9 ,image: 'https://dailymedia.investagrams.com/investadaily/2019/04/Avenger-Traders-2.jpg', title: 'Avenger', description: 'In the years since Tony Stark told the world he was Iron Man, the Marvel Cinematic Universe ...'},
             {id: 10,image: 'https://dailymedia.investagrams.com/investadaily/2019/04/Avenger-Traders-2.jpg', title: 'Avenger', description: 'In the years since Tony Stark told the world he was Iron Man, the Marvel Cinematic Universe ...'},
             {id: 11,image: 'https://dailymedia.investagrams.com/investadaily/2019/04/Avenger-Traders-2.jpg', title: 'Avenger', description: 'In the years since Tony Stark told the world he was Iron Man, the Marvel Cinematic Universe ...'},
             {id: 12,image: 'https://dailymedia.investagrams.com/investadaily/2019/04/Avenger-Traders-2.jpg', title: 'Avenger', description: 'In the years since Tony Stark told the world he was Iron Man, the Marvel Cinematic Universe ...'},
             {id: 13,image: 'https://dailymedia.investagrams.com/investadaily/2019/04/Avenger-Traders-2.jpg', title: 'Avenger', description: 'In the years since Tony Stark told the world he was Iron Man, the Marvel Cinematic Universe ...'},
             {id: 14,image: 'https://dailymedia.investagrams.com/investadaily/2019/04/Avenger-Traders-2.jpg', title: 'Avenger', description: 'In the years since Tony Stark told the world he was Iron Man, the Marvel Cinematic Universe ...'},
             {id: 15,image: 'https://dailymedia.investagrams.com/investadaily/2019/04/Avenger-Traders-2.jpg', title: 'Avenger', description: 'In the years since Tony Stark told the world he was Iron Man, the Marvel Cinematic Universe ...'},
             {id: 16 ,image: 'https://dailymedia.investagrams.com/investadaily/2019/04/Avenger-Traders-2.jpg', title: 'Avenger', description: 'In the years since Tony Stark told the world he was Iron Man, the Marvel Cinematic Universe ...'}]
    }
  }




  render() {
    return (
      <Router>
        <Menu/>
        <Switch>
          <Route path='/' exact component={Main_Page}/>
          <Route path='/home' render={() => <Home data={this.state.data}/>}/>
          <Route path='/video' component={Video}/>
          <Route path='/account' component={Account}/>
          <Route path='/auth' component={Auth}/>
          <Route path='/post/:id' component={Post}/>
          <Route path='/wellcome' component={Wellcome}/>
        </Switch>
      </Router>
  )
  }
}
